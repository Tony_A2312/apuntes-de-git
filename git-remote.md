# git remote add origin https://github.com/TonyCastillo94/apuntes-de-git.git
Con este comando vinculamos nuestro repositorio local con Github.

## Pasos para aportar a otro respositorio

1. Hacer un fork en GitHub.
2. Clonar el repositorio desde mi cuenta de GitHub.
3. Crear una rama local.
4. Realizar mis cambios en mi nueva ramam local.
5. Confirmar los cambios realizados en local.
6. Hacer push de mis cambios (enviar los commits locales a GitHub) `git push origin nombreRama`
7. Crear un pull request con la nueva ramam de mi repositorio en GitHub.
8. Esperar que el administrador del repositorio original acepte mis cambios.