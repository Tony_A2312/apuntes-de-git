# git log
Muestra todo el historial de commits del proyeco.

`git log --pretty=format:"%h - %an, %ar : %s"`
Muestra el historial con el formato que indicamos.

## Limitar la salida del historial
`git log -n` Cambiamos la n por cualquier número entero por ejemplo: `git log -2` nos mostrará los 2 commits más recientes.

`git log --after="2018-04-07 00:00:00"`: Muestra los commits realizados después de la fecha espeficicada.

`git log --before="2018-04-07 00:00:00"`: Muestra los commits realizados antes de la fecha espeficicada.

Las banderas del comando `git log` se pueden usar juntas según convenga, por ejemplo:
`git log --after="2018-01-01 12:00:00"
--before="2018-02-07 12:30:00"`

`git log --oneline`
Este comando nos muestra el historial en una sola línea por commit.
