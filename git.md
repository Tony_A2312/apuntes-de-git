# Crear repositorio

- Puede ser o no una carpeta vacía
- Al iniciar se creara una carpeta .git

Crea un nuevo repositorio Git.
`git init`

# Los 3 estados de Git

1. Espacio de trabajo
2. Area de preparacion
3. Repositorio(carpeta .git)

Sirve para ver el estado actual de mi repositorio.
`git status`

Sirve para mover los archivos del espacio de trabajo al area de preparacion
`git add`

Guarda los cambios en el repositorio.
`git commit -m`

Cambia entre diferentes ramas o versiones del repositorio.
`git checkout`

Crea una rama y de una vez pasa hacia ella
`git checkout -b development`

Elimina la rama
`git branch -D <nombre de la rama>`

Cambia de rama version nueva
`git switch <NombreRama>`

Crea una copia local de un repositorio remoto existente.
`git clone`

Envía cambios al repositorio remoto.
`git push`

Obtiene los cambios del repositorio remoto y los fusiona con la copia local.
`git pull`

Obtiene los cambios del repositorio remoto pero no los fusiona con la copia local.
`git fetch`

Muestra una lista de ramas y crea una nueva rama.
`git branch`

Fusiona una rama con otra.
`git merge`

Muestra una lista de confirmaciones anteriores.
` git log`

Deshace los cambios en el área de preparación o la confirmación anterior.
`git reset`

Deshace una confirmación anterior creando una nueva confirmación.
`git revert`

Agrega una etiqueta a una confirmación específica.
`git tag`

Muestra las diferencias entre dos versiones del repositorio.
`git diff`

Guarda temporalmente los cambios sin realizar una confirmación.
` git stash`

Elimina archivos del repositorio.
`git rm`

Mueve o cambia el nombre de un archivo en el repositorio.
`git mv`

Muestra una lista de repositorios remotos y administra las conexiones a ellos.
`git remote`
