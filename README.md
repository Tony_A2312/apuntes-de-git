# Curso Git desde cero
Sistema de control de versiones para el mantenimiento eficiente y confiable de archivos.

## Zonas de Git
1.Directorio de trabajo
2.Area de preparación
3.Directorio Git

## Flujo de trabajo básico en Git
1.Modificas una serie de archivos en tu directorio de trabajo.
2.Prepara los archivos, añadiéndolos a tu área de preparación.
3.Confirmas los cambios, lo que toma los archivos tal y como están en el área de preparación y almacena esa copia instantánea de manera permanente en tu directorio de Git.

## Configurando Git por primera vez
```
git config --global user.name "Bryan Castillo"
git config --global user.email contactobryan@gmail.com
git config --global core.editor nano
git config --list
```

## Varios repositorios remotos
Podemos configurar un mismo proyecto para sincronizar cambios con varios repositorios remotos.