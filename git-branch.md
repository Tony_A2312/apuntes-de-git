# git branch
Una rama en Git es simplemente un apuntador móvil a uno de los commits.


## git branch --no-merged
Nos muestra cuáles ramas no han sido fusionada a la rama actual.

## git branch --merged
Nos muestra cuáles ramas han sido fusionada a la rama actual.

Puedo crear todas las ramas que quiera y/o necesite.

Las ramas nuevas que se crean apuntan al commit actual.
